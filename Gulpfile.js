const gulp = require('gulp')
const sass = require('gulp-sass')
const babel = require('gulp-babel')
const imagemin = require('gulp-imagemin')
const postcss = require('gulp-postcss')
const postcssPresetEnv = require('postcss-preset-env')
var server = require('gulp-webserver')

function img () 
{
	return gulp
		.src('src/img/*')
		.pipe(imagemin())
		.pipe(gulp.dest('dist/img'))
}

function scss() 
{
	return gulp
		.src('src/sass/**/*.scss')
		.pipe(sass().on('error', sass.logError))
		.pipe(postcss([postcssPresetEnv()]))
		.pipe(gulp.dest('dist/css'))
}

function js () 
{
	return gulp
		.src('src/js/**/*.js')
		.pipe(
			babel({
				presets: ['@babel/env']
			})
		)
		.pipe(gulp.dest('dist/js'))
}

function devServer () 
{
	gulp.src('dist').pipe(
		server({
			livereload: true,
			directoryListing: {
				enable: true,
				path: 'dist'
			},
			open: true,
			port: 3000
		})
	)
}

function watch () 
{
	gulp.watch('src/sass', scss)
	gulp.watch('src/js', js)
}

exports.scss = scss
exports.js = js
exports.watch = watch
exports.server = gulp.series(scss, js, gulp.parallel(devServer, watch))
exports.default = watch
