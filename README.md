#SASS es6 automater
***

## Description

A tool to write es6 code and compile sass with autoprefix and polyfill
Code will be put in dist folder and convert on the fly

___

### Requirement
* Install yarn and nodejs
* run `yarn start`

## Extra
* If in visual studio code add extension eslint and prettier for code formatting and linter

## Todo
* Fix hot reload on files
* Fix formatting on compiled files